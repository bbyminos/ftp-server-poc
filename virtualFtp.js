"use strict";

let
  ftpd = require('ftpd'),
  stream = require('stream'),
  EventEmitter = require('events').EventEmitter,
  util = require('util');

//available events to communicate:
// • new VirtualFtp('user', 'pass', tlsOptions).on('upload', uploadEvents, fileName => {})
//   has:
//    • uploadEvents.on('data', chunkOfStreamData => {})
//    • uploadEvents.emit('error', theReasonWhyImCallingError) <- this will kill the upload and error to the client
//    • uploadEvents.on('finish', () => {}) <- upload is done

module.exports = VirtualFtp;
util.inherits(VirtualFtp, EventEmitter);

function VirtualFtp(username, password, tlsOptions) {

  let
    self = this,
    virtualFsRoot = '/',
    serverOptions = {
      getInitialCwd: () => virtualFsRoot,
      getRoot: () => virtualFsRoot
    };

  if(tlsOptions) {
    serverOptions.allowUnauthorizedTls = true;
    serverOptions.tlsOptions = tlsOptions;
    serverOptions.tlsOnly = true;
  }

  //some FTP clients like to see at least 1 folder or it blows up *shrug*
  let fakeRoot = {
    '/': {
      isDirectory: () => true,
      mode: 20479,
      size: 0,
      mtime: new Date()
    },
    '/temp': {
      isDirectory: () => true,
      mode: 20479,
      size: 0,
      mtime: new Date()
    }
  };

  //fake implementation of FS for our 'virtual filesystem'
  let fakeFs = {
    readdir: (path, cb) =>  {
      var children =
        Object.keys(fakeRoot)
          .filter(x => x.startsWith(path) && x.length !== path.length) || [];

      cb(null, children);
    },
    stat: (path, cb) =>  {
      cb(null, fakeRoot[path]);
    },
    createWriteStream: function(fileName) {
      var passThrough = new stream.PassThrough();

      passThrough
        .on('finish', () => {
          passThrough.emit('close');
          passThrough.emit('end');
        });

      //emulate being a fs.createWriteStream for ftpd
      passThrough.destroy = passThrough.end;
      passThrough.destroySoon = passThrough.end;
      setImmediate(() => {
        //emulate fs saying 'file open, lets roll. this actually starts the process, REQUIRED!
        passThrough.emit('open', fileName);
        self.emit('upload', passThrough, fileName); //an upload is starting tell 3rd party module
      });

      return passThrough;
    }
  };

  (new ftpd.FtpServer('localhost', serverOptions)
    .on('client:connected', function (conn) {
      console.log('Client connected from ' + conn.socket.remoteAddress);
      conn.on('command:user', function (incomingUsername, success, failure) {
        incomingUsername === username ? success() : failure();
      });
      conn.on('command:pass', function (pass, success, failure) {
        pass === password ? success(username, fakeFs) : failure();
      });
    }).listen(4443, function () {
      console.log('FTPD listening on port 4443');
    }));
}