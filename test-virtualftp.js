"use strict";

let
  VirtualFtp = require('./virtualFtp'),
  fs = require('fs'),
  split2 = require('split2');


let ftp = new VirtualFtp('username', 'password', {
  key: fs.readFileSync('private-key.pem'),
  cert: fs.readFileSync('public-cert.pem')
});

ftp.on('upload', (upload, fileName) => {
  console.log('Started upload:', fileName);

  upload
    .pipe(split2())
    .on('data', data => {
      let dataStr = (data || '').toString('utf8');
      console.log('LINE:', dataStr);
      if(dataStr.includes('FAIL')) {
        upload.emit('error', 'found fail line');
      }
    });

  upload.on('finish', () => {
    console.log('Finished upload:', fileName);
  });
});